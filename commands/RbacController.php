<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->createRole('teammember');
		$auth->add($teammember);
		
		$teamleader = $auth->createRole('teamleader');
		$auth->add($teamleader);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionTmpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexLeads = $auth->createPermission('indexLeads');
		$indexLeads->description = 'All users can view leads';
		$auth->add($indexLeads);
		
		$updateOwnLead = $auth->createPermission('updateOwnLead');
		$updateOwnLead->description = 'Team member can update
									only his/her own leads';
		$auth->add($updateOwnLead);

		$viewLead = $auth->createPermission('viewLead');
		$viewLead->description = 'View leads';
		$auth->add($viewLead);

		$updateOwnUser = $auth->createPermission('updateOwnUser');
		$updateOwnUser->description = 'Every user can update his/her
									own profile ';
		$auth->add($updateOwnUser);
		 
		$updateOwnPassword  = $auth->createPermission('updateOwnPassword');
		$updateOwnPassword->description = 'VEvery user can update his/her
									own password';
		$auth->add($updateOwnPassword);		
	}


	public function actionTlpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createLead = $auth->createPermission('createLead');
		$createLead->description = 'Team leader can create new leads';
		$auth->add($createLead);
		
		$updateLead = $auth->createPermission('updateLead');
		$updateLead->description = 'Team leader can update
									leads including assignment';
		$auth->add($updateLead);		
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createUser = $auth->createPermission('createUser');
		$createUser->description = 'Admin can create new users';
		$auth->add($createUser);
		
		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Admin can update all users';
		$auth->add($updateUser);

		$deleteUser = $auth->createPermission('deleteUser');
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);

		$updatePassword = $auth->createPermission('updatePassword');
		$updatePassword->description = 'Admin can update password for 
									all users';
		$auth->add($updatePassword);
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->getRole('teammember');

		$indexLeads = $auth->getPermission('indexLeads');
		$auth->addChild($teammember, $indexLeads);

		$updateOwnLead = $auth->getPermission('updateOwnLead');
		$auth->addChild($teammember, $updateOwnLead);

		$viewLead = $auth->getPermission('viewLead');
		$auth->addChild($teammember, $viewLead);

		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->addChild($teammember, $updateOwnUser);		
		
		$updateOwnPassword = $auth->getPermission('updateOwnPassword');
		$auth->addChild($teammember, $updateOwnPassword);		
		
		$teamleader = $auth->getRole('teamleader');
		$auth->addChild($teamleader, $teammember);
		
		$createLead = $auth->getPermission('createLead');
		$auth->addChild($teamleader, $createLead);

		$updateLead = $auth->getPermission('updateLead');
		$auth->addChild($teamleader, $updateLead);
		
		$admin = $auth->getRole('admin');
		$auth->addChild($admin, $teamleader);		
		
		$createUser = $auth->getPermission('createUser');
		$auth->addChild($admin, $createUser);

		$updateUser = $auth->getPermission('updateUser');
		$auth->addChild($admin, $updateUser);
		
		$deleteUser = $auth->getPermission('deleteUser');
		$auth->addChild($admin, $deleteUser);		

		$updatePassword = $auth->getPermission('updatePassword');
		$auth->addChild($admin, $updatePassword);
	}
}