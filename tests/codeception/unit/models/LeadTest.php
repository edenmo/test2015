<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use app\models\Lead;
use Codeception\Specify;

class LeadTest extends TestCase
{
    use Specify;

    protected function setUp()
    {
        parent::setUp();
		$model = new Lead([
            'name' => 'Nate Robinson',
            'email' => 'nate@nba.com',		
		]);
		$model->save();
    }

    protected function tearDown()
    {
		$model = Lead::find()
		->where(['email' => 'nate@nba.com'])
		->one();
		$model->delete(); 
    }	

	
    public function testExistLead()
    {
		$model = Lead::find()
		->where(['email' => 'nate@nba.com'])
		->one();

        $this->specify('Lead Should Exist in DB', function () use ($model) {
            expect('Name should be correct', $model->name=='Nate Robinson')->true();

        });
    }
	
	
	
}
