<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (\Yii::$app->user->can('createUser')) { ?>
    <p>

		<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>	
    </p>
	<?php } ?>	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'password',
            'auth_key',
            'firstname',
            'lastname',
            'email:email',
            'phone',
			[ // The role of the user
				'label' => $model->attributeLabels()['role'],
				'value' => $model->userole,
			],			
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>
	
	<?php var_dump(Yii::$app->authManager->getRolesByUser($model->id)) ?>

</div>
