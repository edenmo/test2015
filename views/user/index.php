<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if (\Yii::$app->user->can('createUser')) { ?>
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            //'password',
            //'auth_key',
            'firstname',
            'lastname',
            'email:email',
            'phone',
            [
				//'attribute' => 'owner',
				'label' => 'Role',
				'format' => 'raw',
				'value' => function($model){
					return array_keys(\Yii::$app->authManager->getRolesByUser($model->id))[0];					
				},
				'filter'=>Html::dropDownList('UserSearch[role]', $role, $roles, ['class'=>'form-control']),
			],			
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
