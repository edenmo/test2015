<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Status1;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoryId')->dropDownList(Category::getCategories()) ?>
<?php if (!$model->isNewRecord): ?>
    <?= $form->field($model, 'statusId')->
				dropDownList(Status1::getStatuses()) ?>
<?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
