<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "status1".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Activity[] $activities
 */
class Status1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'name');
		return $allStatusesArray;						
	}
    public static function tableName()
    {
        return 'status1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['statusId' => 'id']);
    }
	public function getName()
	{
		return $this->name;
	}
}
