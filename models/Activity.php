<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 *
 * @property Category $category
 * @property Status1 $status
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'categoryId'], 'required'],
		
            [['categoryId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
            [['statusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status1::className(), 'targetAttribute' => ['statusId' => 'id']],
        ];
    }
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
    }
public function getCategoryItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status1::className(), ['id' => 'statusId']);
    }
	public function beforeSave($insert)
    {
       // $return = parent::beforeSave($insert);

     //   if ($this->isAttributeChanged('statusId'))
           return $this->statusId = "2";

       
    }
}
