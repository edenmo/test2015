<?php

use yii\db\Migration;

class m160718_152234_alter_user_table extends Migration
{
    public function up()
    {
$this->addColumn('user','firstname','string');
$this->addColumn('user','lastname','string');
$this->addColumn('user','email','string');
$this->addColumn('user','phone','string');
$this->addColumn('user','created_at','date');
$this->addColumn('user','updated_at','date');
$this->addColumn('user','created_by','string');
$this->addColumn('user','updated_by','string');
    }

    public function down()
    {
       $this->dropTable('user');

        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
