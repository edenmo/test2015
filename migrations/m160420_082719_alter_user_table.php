<?php

use yii\db\Migration;

class m160420_082719_alter_user_table extends Migration
{
    public function up()
    {
		$this->addColumn('user','firstname','string');
		$this->addColumn('user','lastname','string');
		$this->addColumn('user','email','string');
		$this->addColumn('user','phone','string');
		$this->addColumn('user','created_at','integer');
		$this->addColumn('user','updated_at','integer');
		$this->addColumn('user','created_by','integer');
		$this->addColumn('user','updated_by','integer');
    }

    public function down()
    {
        $this->dropColumn('user','firstname');
		$this->dropColumn('user','lastname');
		$this->dropColumn('user','email');
		$this->dropColumn('user','phone');
		$this->dropColumn('user','created_at');
		$this->dropColumn('user','updated_at');
		$this->dropColumn('user','created_by');
		$this->dropColumn('user','updated_by');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
