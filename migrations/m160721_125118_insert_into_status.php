<?php

use yii\db\Migration;

class m160721_125118_insert_into_status extends Migration
{
    public function up()
    {
$this->insert('status',[
	'name'=>'Lead',
	]);
	$this->insert('status',[
	'name'=>'Qualified lead',
	]);
	$this->insert('status',[
	'name'=>'Customer',
	]);
    }

    public function down()
    {
        echo "m160721_125118_insert_into_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
