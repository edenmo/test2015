<?php
use yii\db\Migration;
class m160402_080613_init_demo_table extends Migration
{
    public function up()
    {
        $this->createTable(
            'demo',
            [
                'id' => 'pk',
                'name' => 'string',
                'demo_date' => 'date',
                'notes' => 'text',
            ],
            'ENGINE=InnoDB'
        );
    }
    public function down()
    {
        $this->dropTable('demo');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
