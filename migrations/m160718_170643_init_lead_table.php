<?php

use yii\db\Migration;

class m160718_170643_init_lead_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'lead',
			[
				'id'=>'pk',
				'name' => 'string',
				'email' => 'string',
				'notes' => 'text',
				'status' => 'integer',
				'owner' => 'string',
				'created_at' => 'date',
				'updated_at' => 'date',
				'created_by' => 'string',
				'updated_by' => 'string'
				
			],
			'ENGINE=InnoDB'
		);
    }

    public function down()
    {
		$this->dropTable('lead');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
